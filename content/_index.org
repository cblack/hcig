#+title: Preface

* About This Document

HIG NG describes how to create software that's efficient and enjoyable to use.

This document also contains examples of how not to design a human-computer interface; these examples are marked as being examples of what you shouldn't do, and explain what's wrong, and how you correct the flaws.

* Who Should Read This Document

This document is for people who design and develop software.
If you are a designer, a developer, or just an interested user, this document is for you.

Even if you don't design and develop software, this document will help you understand the underpinnings of computer interfaces.

We assume you're familiar with the concepts and words presented in common computer interfaces, and that you have experience using them.