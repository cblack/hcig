#!/usr/bin/sh

while true; do
    inotifywait -e close_write,moved_to,create layouts/**

    mv assets/css/main.css assets/css/main.css.
    mv assets/css/main.css. assets/css/main.css
done
