module.exports = {
    content: ['layouts/*/*.html'],
    darkMode: 'media', // or 'media' or 'class'
    theme: {
      extend: {},
      fontFamily: {
        'display': ['Inter', 'system-ui'],
      },
    },
    variants: {
      extend: {},
    },
    plugins: [
      require('@tailwindcss/typography')
    ],
  }
  